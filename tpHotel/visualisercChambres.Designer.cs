﻿namespace tpHotel
{
    partial class visualiserChambres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstChambre = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.lstChambre)).BeginInit();
            this.SuspendLayout();
            // 
            // lstChambre
            // 
            this.lstChambre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstChambre.Location = new System.Drawing.Point(183, 106);
            this.lstChambre.Name = "lstChambre";
            this.lstChambre.Size = new System.Drawing.Size(470, 216);
            this.lstChambre.TabIndex = 0;
            // 
            // visualiserChambres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lstChambre);
            this.Name = "visualiserChambres";
            this.Text = "visualiserChambres";
            this.Load += new System.EventHandler(this.visualiserChambres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lstChambre)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView lstChambre;
    }
}