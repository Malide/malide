﻿namespace tpHotel
{
    partial class frmAccueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAjoutHotel = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnVoirHotel = new System.Windows.Forms.Button();
            this.AjouterChambres = new System.Windows.Forms.Button();
            this.visualiserChambres = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAjoutHotel
            // 
            this.btnAjoutHotel.Location = new System.Drawing.Point(62, 45);
            this.btnAjoutHotel.Name = "btnAjoutHotel";
            this.btnAjoutHotel.Size = new System.Drawing.Size(145, 35);
            this.btnAjoutHotel.TabIndex = 0;
            this.btnAjoutHotel.Text = "Ajout hôtels";
            this.btnAjoutHotel.UseVisualStyleBackColor = true;
            this.btnAjoutHotel.Click += new System.EventHandler(this.btnAjoutHotel_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(183, 272);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(79, 30);
            this.btnQuitter.TabIndex = 1;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnVoirHotel
            // 
            this.btnVoirHotel.Location = new System.Drawing.Point(86, 86);
            this.btnVoirHotel.Name = "btnVoirHotel";
            this.btnVoirHotel.Size = new System.Drawing.Size(95, 35);
            this.btnVoirHotel.TabIndex = 1;
            this.btnVoirHotel.Text = "Visualiser hôtels";
            this.btnVoirHotel.UseVisualStyleBackColor = true;
            this.btnVoirHotel.Click += new System.EventHandler(this.btnVoirHotel_Click);
            // 
            // AjouterChambres
            // 
            this.AjouterChambres.Location = new System.Drawing.Point(62, 139);
            this.AjouterChambres.Name = "AjouterChambres";
            this.AjouterChambres.Size = new System.Drawing.Size(145, 40);
            this.AjouterChambres.TabIndex = 2;
            this.AjouterChambres.Text = "AjouterChambres";
            this.AjouterChambres.UseVisualStyleBackColor = true;
            this.AjouterChambres.Click += new System.EventHandler(this.VisualiserChambres_Click);
            // 
            // visualiserChambres
            // 
            this.visualiserChambres.Location = new System.Drawing.Point(86, 206);
            this.visualiserChambres.Name = "visualiserChambres";
            this.visualiserChambres.Size = new System.Drawing.Size(95, 31);
            this.visualiserChambres.TabIndex = 3;
            this.visualiserChambres.Text = "visualiserChambres";
            this.visualiserChambres.UseVisualStyleBackColor = true;
            this.visualiserChambres.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 346);
            this.Controls.Add(this.visualiserChambres);
            this.Controls.Add(this.AjouterChambres);
            this.Controls.Add(this.btnVoirHotel);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnAjoutHotel);
            this.Name = "frmAccueil";
            this.Text = "Accueil";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAjoutHotel;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnVoirHotel;
        private System.Windows.Forms.Button AjouterChambres;
        private System.Windows.Forms.Button visualiserChambres;
    }
}