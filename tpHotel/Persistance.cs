﻿using System;
using System.Data.SqlClient;
using System.Collections;

namespace tpHotel
{
    static class Persistance
    {
        private static SqlConnection connexionSql()
        {
            string server = "192.168.1.28";
            string database = "hotelBuffet";
            string uid = "lmct65";
            string password = "lmct65";

            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";";
            connectionString += "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            SqlConnection conn = new SqlConnection(connectionString);

            return conn;
        }


        public static void ajouteHotel(String nom, String adresse, String ville)
        {
            SqlConnection conn = Persistance.connexionSql();
            conn.Open();
            string requete = "insert into hotel (nom,adresse,ville) ";
            requete += "Values('" + nom + "','" + adresse + "','" + ville + "')";
            SqlCommand cmd = new SqlCommand(requete, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static ArrayList getLesHotels()
        {
            ArrayList liste = new ArrayList();
            SqlConnection conn = Persistance.connexionSql();
            conn.Open();
            string requete = ("select * from hotel");
            SqlCommand cmd = new SqlCommand(requete, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                liste.Add(new Hotel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }
            conn.Close();
            return liste;
        }


        public static ArrayList getLeschambres()
        {
            ArrayList liste = new ArrayList();
            SqlConnection conn = Persistance.connexionSql();
            conn.Open();
            string requete = ("select * from Chambre");
            SqlCommand cmd = new SqlCommand(requete, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                liste.Add(new Room(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetString(3)));
            }
            conn.Close();
            return liste;
        }

    }
}