﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Room
    {
        private int id;
        private int idHotel;
        private int etage;
        private string description;

        public Room(int id, int idHotel, int etage, string description)
        {
            this.id = id;
            this.IdHotel = idHotel;
            this.Etage = etage;
            this.Description= description;
        }

        public int Id { get => id; set => id = value; }
        public int IdHotel { get => idHotel; set => idHotel = value; }
        public int Etage { get => etage; set => etage = value; }
        public string Description { get => description; set => description = value; }
    }
}
