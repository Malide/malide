﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;

        // construction du constructeur 
        public Hotel(int unid, string unnom, string uneadresse, string uneville)
        {
            Id = unid;
            nom = unnom;
            Adresse = uneadresse;
            Ville = uneville;


        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
}
