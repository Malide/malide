﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class Chambre : Form
    {
        public Chambre()
        {
            InitializeComponent();
        }

        private void Chambre_Load(object sender, EventArgs e)
        {
            ArrayList liste = Persistance.getLesHotels();
            foreach (Hotel chambre in liste) 
            {
                lstHotel.Items.Add(chambre.Id);

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Persistance.ajouteHotel(Etage.Text, Description.Text, lstHotel.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
           
            this.Etage.Text = "";
            this.Description.Text = "";
            this.lstHotel.Text = "";
       
        }
    }
}
